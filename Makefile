PROJECT := $(shell gcloud config get-value project)
PROJECT := grpc-test-194507
CLUSTER := test-cluster-1
ZONE    := asia-northeast1-a

IMAGE_TAG := gcr.io/$(PROJECT)/grpc-echo

SERVICE_LB_ADDR = $(shell kubectl get service service-lb --output jsonpath="{.status.loadBalancer.ingress[0].ip}")
INGRESS_LB_ADDR = $(shell kubectl get ingress ingress-lb --output jsonpath="{.status.loadBalancer.ingress[0].ip}")

all: echo.pb.go client/echo.pb.go

echo.pb.go: echo.proto
	protoc \
		-I=. \
		--go_out=plugins=grpc:. \
		$?

client/echo.pb.go: echo.pb.go
	cp echo.pb.go client

serve:
	go run *.go

test-local:
	cd client && go run *.go ${ARGS}

test-service-lb:
	make test-local ARGS="-addr=$(SERVICE_LB_ADDR):80"

test-ingress-lb:
	make test-local ARGS="-addr=$(INGRESS_LB_ADDR):80"

docker-build:
	docker build -t $(IMAGE_TAG) .

docker-push:
	gcloud docker -- push $(IMAGE_TAG)

get-credentials:
	gcloud container clusters get-credentials $(CLUSTER) --zone $(ZONE)

k8s-deployment:
	-kubectl delete -f k8s/deployment.yaml
	kubectl create -f k8s/deployment.yaml

k8s-service-lb:
	kubectl apply -f k8s/service-lb.yaml

k8s-ingress-lb:
	kubectl apply -f k8s/ingress-lb.yaml
