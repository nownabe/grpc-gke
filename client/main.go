package main

import (
	"flag"
	"fmt"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	msg = "Hello, gRPC!"
)

func main() {
	var addr string
	flag.StringVar(&addr, "addr", "127.0.0.1:50051", "server address")
	flag.Parse()

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	c := NewEchoServiceClient(conn)

	ctx := context.Background()
	req := &Request{Message: msg}

	if res, err := c.Echo(ctx, req); err == nil {
		fmt.Println("Succeeded:", res.Message)
	} else {
		fmt.Println("Failed:", err)
	}
}
