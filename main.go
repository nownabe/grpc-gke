package main

import (
	"fmt"
	"net"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

type server struct {
	loc *time.Location
}

func newServer() server {
	loc, err := time.LoadLocation("Asia/Tokyo")
	if err != nil {
		panic(err)
	}
	return server{loc: loc}
}

func (s server) Echo(ctx context.Context, in *Request) (*Response, error) {
	n := time.Now().In(s.loc)
	fmt.Println("Received:", in.Message, "at", n)
	return &Response{Message: in.Message}, nil
}

func main() {
	s := grpc.NewServer()
	RegisterEchoServiceServer(s, newServer())

	lis, err := net.Listen("tcp", port)
	if err != nil {
		panic(err)
	}

	fmt.Println("Running...")

	if err := s.Serve(lis); err != nil {
		panic(err)
	}
}
